﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UD5T2.MVVM.Model;

namespace UD5T2.MVVM.ViewModel
{
    // Se define una clase llamada BMIViewModel.
    public class BMIViewModel
    {
        // Se declara una instancia de la clase BMI como campo público.
        public BMI bmi;

        // Se define una propiedad BMI con un bloque get y set.
        public BMI BMI
        {
            get; set;
        }

        // Constructor de la clase BMIViewModel.
        public BMIViewModel()
        {
            // Se inicializa la propiedad BMI con una nueva instancia de la clase BMI.
            // Se establecen valores predeterminados para las propiedades Peso y Altura.
            BMI = new BMI { Peso = 50, Altura = 25 };
        }
    }
}