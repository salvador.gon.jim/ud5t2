﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UD5T2.MVVM.Model
{
    // Se define una clase llamada BMI que implementa la interfaz INotifyPropertyChanged.
    public class BMI : INotifyPropertyChanged
    {
        // Se declaran las variables de instancia privadas para almacenar la altura, peso, resultado y resultadoBMI.
        private float altura;
        private float peso;
        private float resultado;
        private string resultadoBMI;

        // Propiedad Altura con un bloque get y set que implementa la notificación de cambio de propiedad.
        public float Altura
        {
            get { return altura; }
            set
            {
                if (altura != value)
                {
                    altura = value;
                    OnPropertyChanged(nameof(altura));
                    OnPropertyChanged(nameof(resultado));
                    OnPropertyChanged(nameof(resultadoBMI));
                }
            }
        }

        // Propiedad Peso con un bloque get y set que implementa la notificación de cambio de propiedad.
        public float Peso
        {
            get { return peso; }
            set
            {
                if (peso != value)
                {
                    peso = value;
                    OnPropertyChanged(nameof(peso));
                    OnPropertyChanged(nameof(resultadoBMI));
                }
            }
        }

        // Propiedad ResultadoBMI que calcula el resultado BMI y devuelve una cadena de acuerdo con los intervalos de BMI.
        public string ResultadoBMI
        {
            get
            {
                float resultado = Resultado;

                switch (resultado)
                {
                    // Se utiliza un switch para definir diferentes mensajes según el resultado BMI.
                    // Cada caso compara el resultado con un valor específico.
                    case var _ when resultado <= 16:
                        return "BMI: Delgado Severo";
                    case var _ when resultado <= 17:
                        return "BMI: Delgado Moderado";
                    case var _ when resultado <= 18.5:
                        return "BMI: Delgado Medio";
                    case var _ when resultado <= 25:
                        return "BMI: Normal";
                    case var _ when resultado <= 30:
                        return "BMI: Sobrepeso";
                    case var _ when resultado <= 35:
                        return "BMI: Obesidad Clase I";
                    case var _ when resultado <= 40:
                        return "BMI: Obesidad Clase II";
                    default:
                        return "BMI: Obesidad Clase III";
                }
            }
        }

        // Propiedad Resultado que calcula el resultado BMI mediante la fórmula Peso / (Altura * Altura) * 10000.
        public float Resultado
        {
            get { return Peso / (Altura * Altura) * 10000; }
        }

        // Evento PropertyChanged que se activa cuando cambia una propiedad.
        public event PropertyChangedEventHandler? PropertyChanged;

        // Método protegido OnPropertyChanged que dispara el evento PropertyChanged.
        // Se utiliza el atributo CallerMemberName para obtener automáticamente el nombre de la propiedad que cambió.
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}