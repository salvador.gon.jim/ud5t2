using UD5T2.MVVM.ViewModel;

namespace UD5T2.MVVM.Views
{
    // Se define una clase parcial llamada BMIView que hereda de ContentPage.
    public partial class BMIView : ContentPage
    {
        // Constructor de la clase BMIView.
        public BMIView()
        {
            // Se llama al m�todo InitializeComponent() para inicializar los componentes de la vista.
            InitializeComponent();

            // Se crea una nueva instancia de BMIViewModel y se asigna como contexto de enlace (BindingContext) para la vista.
            this.BindingContext = new BMIViewModel();
        }
    }
}
